VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Cls_GetPing"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' WSock32 Variables
Dim Ret As Long, sLowByte As String, sHighByte As String
Dim sMsg As String, HostLen As Long, Host As String
Dim Hostent As Hostent, PointerToPointer As Long, ListAddress As Long
Dim WSAdata As WSAdata, DotA As Long, DotAddr As String, ListAddr As Long
Dim MaxUDP As Long, MaxSockets As Long, i As Integer
Dim Descripcion As String, status As String
' ICMP Variables
Dim bReturn As Boolean, hIP As Long
Dim szBuffer As String
Dim Addr As Long
Dim error As String
Dim HostRespuesta As String
Dim TraceRT As Boolean
Dim TTL As Integer
'Variables para las propiedaes
Public m_NombreHost As String 'Nombre del Host
Public m_IPHost As String 'Ip del Host
Public m_nPaquetes As Byte 'Paquetes a enviar
'si se produce un error se manda al control de salida
Private Sub LeerError(ControlSalida As Object)
    Dim ControlSalida2 As String
    If pIPe.status = 0 Then error = "Success"
    If pIPe.status = 11001 Then error = "Buffer too Small"
    If pIPe.status = 11002 Then error = "Dest Network Not Reachable"
    If pIPe.status = 11003 Then error = "Dest Host Not Reachable"
    If pIPe.status = 11004 Then error = "Dest Protocol Not Reachable"
    If pIPe.status = 11005 Then error = "Dest Port Not Reachable"
    If pIPe.status = 11006 Then error = "No Resources Available"
    If pIPe.status = 11007 Then error = "Bad Option"
    If pIPe.status = 11008 Then error = "Hardware Error"
    If pIPe.status = 11009 Then error = "Packet too Big"
    If pIPe.status = 11010 Then error = "Rqst Timed Out"
    If pIPe.status = 11011 Then error = "Bad Request"
    If pIPe.status = 11012 Then error = "Bad Route"
    If pIPe.status = 11013 Then error = "TTL Exprd in Transit"
    If pIPe.status = 11014 Then error = "TTL Exprd Reassemb"
    If pIPe.status = 11015 Then error = "Parameter Problem"
    If pIPe.status = 11016 Then error = "Source Quench"
    If pIPe.status = 11017 Then error = "Option too Big"
    If pIPe.status = 11018 Then error = " Bad Destination"
    If pIPe.status = 11019 Then error = "Address Deleted"
    If pIPe.status = 11020 Then error = "Spec MTU Change"
    If pIPe.status = 11021 Then error = "MTU Change"
    If pIPe.status = 11022 Then error = "Unload"
    If pIPe.status = 11050 Then error = "General Failure"
    error = error + " (" + CStr(pIPe.status) + ")"
    DoEvents
'   SE COMENTA PARA QUE NO RETORNE TEXTO CON RESULTADO
    If TraceRT = False Then
        If pIPe.status = 0 Then
            ControlSalida2 = ControlSalida2 + "  Replica de " + _
            HostRespuesta + ":Bytes = " + Trim$(CStr(pIPe.DataSize)) _
            + " RTT = " + Trim$(CStr(pIPe.RoundTripTime)) + "ms TTL = " + _
            Trim$(CStr(pIPe.Options.TTL)) + Chr$(13) + Chr$(10)
        Else
            ControlSalida2 = ControlSalida2 + "  Replica de " + _
            HostRespuesta + ": " + error + Chr$(13) + Chr$(10)
        End If
    Else
        If TTL - 1 < 10 Then ControlSalida2 = ControlSalida2 & "  Hop # 0" + _
        CStr(TTL - 1) Else ControlSalida2 = ControlSalida2 + "  Hop # " + _
        CStr(TTL - 1)
        ControlSalida2 = ControlSalida2 + "  " + HostRespuesta + Chr$(13) + _
        Chr$(10)
    End If
End Sub
'Recupera la Ip a partir del nombre de Host
Public Function vbGetHostByName(m_NombreHost As String) As Integer
    Dim szString As String
    Host = Trim$(m_NombreHost)
    szString = String(64, &H0)
    Host = Host + Right$(szString, 64 - Len(Host))
    If gethostbyname(Host) = SOCKET_ERROR Then
        sMsg = "Winsock Error" & Str$(WSAGetLastError())
        
        m_IPHost = sMsg
        
        '        MsgBox sMsg, vbOKOnly, "Error"
    Else
        PointerToPointer = gethostbyname(Host)
        CopyMemory Hostent.h_name, ByVal PointerToPointer, Len(Hostent)
        ListAddress = Hostent.h_addr_list
        CopyMemory ListAddr, ByVal ListAddress, 4
        CopyMemory IPLong, ByVal ListAddr, 4
        CopyMemory Addr, ByVal ListAddr, 4
        m_IPHost = Trim$(CStr(Asc(IPLong.Byte4)) + "." + CStr(Asc(IPLong.Byte3)) + "." + CStr(Asc(IPLong.Byte2)) + "." + CStr(Asc(IPLong.Byte1)))
    End If
End Function
'Recupera el nombre de Host
Private Sub vbGetHostName()
    Host = String(64, &H0)
    If gethostname(Host, HostLen) = SOCKET_ERROR Then
        sMsg = "WSock32 Error" & Str$(WSAGetLastError())
        MsgBox sMsg, vbOKOnly, "Error"
    Else
        Host = Left$(Trim$(Host), Len(Trim$(Host)) - 1)
        m_NombreHost = Host
    End If
End Sub
Private Sub vbIcmpSendEcho(ControlSalida As Object)
    Dim NbrOfPkts As Integer
    szBuffer = "abcdefghijklmnopqrstuvwabcdefghijklmnopqrstu" & _
    "vwabcdefghijklmnopqrstuvwabcdefghijklmnopqrstuvwabcdefghij" & _
    "klmnopqrstuvwabcdefghijklm"
    szBuffer = Left$(szBuffer, Val("32"))
    If IsNumeric(m_nPaquetes) Then
        If Val(m_nPaquetes) < 1 Then m_nPaquetes = "1"
    Else
        m_nPaquetes = "1"
    End If
    If TraceRT = True Then m_nPaquetes = "1"
    For NbrOfPkts = 1 To Trim$(m_nPaquetes)
        DoEvents
        bReturn = IcmpSendEcho(hIP, Addr, szBuffer, Len(szBuffer), pIPo, pIPe, Len(pIPe) + 8, 2700)
        If bReturn Then
            HostRespuesta = CStr(pIPe.Address(0)) + "." + CStr(pIPe.Address(1)) _
            + "." + CStr(pIPe.Address(2)) + "." + CStr(pIPe.Address(3))
            LeerError ControlSalida
        Else
            If TraceRT Then
                TTL = TTL - 1
            Else    ' Tiempo agotado
'   COMENTADO PARA QUE NO TRAIGA TEXTO DE SALIDA
'                ControlSalida = ControlSalida & _
'                "Tiempo de espera agotado para la solicitud" & Chr$(13) + Chr$(10)
            End If
        End If
    Next NbrOfPkts
If Not TraceRT Then
'   COMENTADO PARA QUE NO TRAIGA TEXTO DE SALIDA
'   ControlSalida = ControlSalida & Chr(13) & Chr(10) & "-----------------" & Chr(13) & Chr(10)
End If
End Sub
'Inicializa el Wsock32
Private Sub vbWSAStartup()
    Ret = WSAStartup(&H101, WSAdata)
    If Ret <> 0 Then
        MsgBox "WSock32.dll no responde!", vbOKOnly, "VB4032-ICMPEcho"
    End If
    If LoByte(WSAdata.wVersion) < WS_VERSION_MAJOR Or _
         (LoByte(WSAdata.wVersion) = WS_VERSION_MAJOR _
          And HiByte(WSAdata.wVersion) < WS_VERSION_MINOR) Then
        
        sHighByte = Trim$(Str$(HiByte(WSAdata.wVersion)))
        sLowByte = Trim$(Str$(LoByte(WSAdata.wVersion)))
        
        sMsg = "Version de WinSock  " & sLowByte & "." & sHighByte
        sMsg = sMsg & " No soportado "
        MsgBox sMsg, vbOKOnly, "Error"
        Exit Sub
    End If
    If WSAdata.iMaxSockets < MIN_SOCKETS_REQD Then
        sMsg = "Esta aplicaci�n requiere un minimo de "
        sMsg = sMsg & Trim$(Str$(MIN_SOCKETS_REQD)) & " sockets soportados."
        MsgBox sMsg, vbOKOnly, "Error"
        Exit Sub
    End If
    MaxSockets = WSAdata.iMaxSockets
    If MaxSockets < 0 Then
        MaxSockets = 65536 + MaxSockets
    End If
    MaxUDP = WSAdata.iMaxUdpDg
    If MaxUDP < 0 Then
        MaxUDP = 65536 + MaxUDP
    End If
    Descripcion = ""
    For i = 0 To WSADESCRIPTION_LEN
        If WSAdata.szDescription(i) = 0 Then Exit For
        Descripcion = Descripcion + Chr$(WSAdata.szDescription(i))
    Next i
    status = ""
    For i = 0 To WSASYS_STATUS_LEN
        If WSAdata.szSystemStatus(i) = 0 Then Exit For
        status = status + Chr$(WSAdata.szSystemStatus(i))
    Next i
End Sub
Private Function HiByte(ByVal wParam As Integer)
    HiByte = wParam \ &H100 And &HFF&
End Function
Private Function LoByte(ByVal wParam As Integer)
    LoByte = wParam And &HFF&
End Function
Private Sub vbWSACleanup()
    Ret = WSACleanup()
    If Ret <> 0 Then
        sMsg = "WSock32 Error - " & Trim$(Str$(Ret)) & " occurred in Cleanup"
        MsgBox sMsg, vbOKOnly, "Error"
        Exit Sub
    End If
End Sub
Private Sub vbIcmpCloseHandle()
    bReturn = IcmpCloseHandle(hIP)
    If bReturn = False Then
        MsgBox "Error", vbOKOnly, "Error"
    End If
End Sub
Private Sub vbIcmpCreateFile()
    hIP = IcmpCreateFile()
    If hIP = 0 Then
        MsgBox "Unable to Create File Handle", vbOKOnly, "VBPing32"
    End If
End Sub
Public Sub Ping(Optional ControlSalida As Object, Optional TTL As Byte = 255)
   'Initializa el Winsock
    vbWSAStartup
    If Len(m_NombreHost) = 0 Then
        vbGetHostName
    End If
    If m_NombreHost = "" Then
        MsgBox "No se especifico un nombre de Host!", vbOKOnly, "Error"
        vbWSACleanup
        Exit Sub
    End If
   'Recupera la IP del Host
    vbGetHostByName m_NombreHost
    vbIcmpCreateFile
    If IsNumeric(TTL) Then
        If (Val(TTL) > 255) Then TTL = "255"
        If (Val(TTL) < 2) Then TTL = "2"
    Else
        TTL = "255"
    End If
    pIPo.TTL = Trim$(CStr(TTL))
    'Env�a el ICMP Echo al control de salida
    vbIcmpSendEcho ControlSalida
    'Cierra ICMP Handle
    vbIcmpCloseHandle
   ' Finaliza el Winsock
    vbWSACleanup
End Sub
Public Sub Trace(ControlSalida As Object, Optional TTL As Byte = 255)
    'Initializa el Winsock
    vbWSAStartup
    If Len(m_NombreHost) = 0 Then
        vbGetHostName
    End If
    If m_NombreHost = "" Then
        MsgBox "No se especifico un Host!", vbOKOnly, "Error"
        vbWSACleanup
        Exit Sub
    End If
    vbGetHostByName m_NombreHost
    'recupera el Handle ICMP
    vbIcmpCreateFile
    TraceRT = True
    'Mostramos los resultados en el control de salida
    ControlSalida = ControlSalida + "Tracing Route to " + m_IPHost + _
    ":" + Chr$(13) + Chr$(10) + Chr$(13) + Chr$(10)
    For TTL = 2 To 255
        pIPo.TTL = TTL
        ''Mostramos los resultados en el control de salida (ICMP Echo Request)
        vbIcmpSendEcho ControlSalida
        DoEvents
        If HostRespuesta = m_IPHost Then
            ControlSalida = ControlSalida + Chr$(13) + Chr$(10) + _
            "Route Trace finalizado" + Chr$(13) + Chr$(10) + Chr$(13) + Chr$(10)
            Exit For
        End If
    Next TTL
    TraceRT = False
    ' Cierra the ICMP Handle
    vbIcmpCloseHandle
    ' Finaliza el Winsock
    vbWSACleanup
End Sub
'Para el nomber del Host
Public Property Get NombreHost() As String
NombreHost = m_NombreHost
End Property
Public Property Let NombreHost(ByVal vNewValue As String)
m_NombreHost = vNewValue
End Property
'Almacena la Ip del Host
Public Property Get IPHost() As Variant
IPHost = m_IPHost
End Property
Public Property Let IPHost(ByVal vNewValue As Variant)
MsgBox "La propiedad es de solo lectura", vbInformation
End Property
'Almacena la Cantidad de paquetes a enviar
Public Property Get Npaquetes() As Byte
Npaquetes = m_nPaquetes
End Property
Public Property Let Npaquetes(ByVal vNewValue As Byte)
m_nPaquetes = vNewValue
End Property




