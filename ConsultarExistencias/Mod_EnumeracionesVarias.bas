Attribute VB_Name = "Mod_EnumeracionesVarias"
Public Enum FindFileConstants
    NO_SEARCH_MUST_FIND_EXACT_MATCH = -1
    SEARCH_ALL_UPPER_LEVELS = 0
    SEARCH_1_UPPER_LEVEL = 1
    SEARCH_2_UPPER_LEVELS = 2
    SEARCH_3_UPPER_LEVELS = 3
    SEARCH_N_INPUT_ANY_NUMBER = 4
    '...
End Enum

Public Enum OperatingSystemArchitecture
    [32Bits]
    [64Bits]
    [OperatingSystemArchitecture_Count]
End Enum

Enum BP_PhoneServices_ProductType ' PrepaidBlackstone
    [BP_PhoneServices_ProductTypeLBound] = 0
    TypeLongDistance = 0
    TypePinless = 5
    TypeDomesticTopup = 6
    TypeInternationalTopup = 7
    TypeSunpass = 9
    [BP_PhoneServices_ProductTypeCount] = 4
    [BP_PhoneServices_ProductTypeUBound] = 9
End Enum

Public Enum RNC_TipoServicio
    [RNC_TipoServicioLBound] = 0
    RecargaGeneral = 1
    [RNC_TipoServicioCount] = 1
    [RNC_TipoServicioUBound] = 0
End Enum

Enum BP_ServiceType
    [BP_ServiceTypeLBound]
    BPSTypePhoneTopUp
    BPSTypeBillPayments
    BPSTypeTolls
    [BP_ServiceTypeCount]
End Enum

Public Enum FechaBDPrecision
    FBD_Fecha
    FBD_FechaYMinuto
    FBD_FULL
    FBD_HoraYMinuto
    FBD_HoraFull
End Enum
