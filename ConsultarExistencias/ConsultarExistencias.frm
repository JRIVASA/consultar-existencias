VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form ConsultarExistencias 
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   12705
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   15360
   Icon            =   "ConsultarExistencias.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   12705
   ScaleWidth      =   15360
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12435
         TabIndex        =   4
         Top             =   105
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   75
         Width           =   5295
      End
      Begin VB.Image Exit 
         Height          =   480
         Left            =   14640
         MouseIcon       =   "ConsultarExistencias.frx":628A
         MousePointer    =   99  'Custom
         Picture         =   "ConsultarExistencias.frx":6594
         Top             =   0
         Width           =   480
      End
   End
   Begin VB.Frame FrameSelect 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   7560
      TabIndex        =   13
      Top             =   0
      Visible         =   0   'False
      Width           =   1575
      Begin VB.Image CmdSelect 
         Height          =   300
         Left            =   480
         Picture         =   "ConsultarExistencias.frx":8316
         Top             =   120
         Width           =   900
      End
   End
   Begin VB.Frame FrameInfo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   5400
      TabIndex        =   12
      Top             =   0
      Visible         =   0   'False
      Width           =   1215
      Begin VB.Image CmdInfo 
         Height          =   300
         Left            =   120
         Picture         =   "ConsultarExistencias.frx":869A
         Top             =   120
         Width           =   900
      End
   End
   Begin VB.Timer Tim_Progreso 
      Enabled         =   0   'False
      Interval        =   500
      Left            =   12600
      Top             =   4200
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " Buscar "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   1095
      Left            =   120
      TabIndex        =   6
      Top             =   1680
      Width           =   13815
      Begin VB.ComboBox cmbItemBusqueda 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         ItemData        =   "ConsultarExistencias.frx":89F3
         Left            =   240
         List            =   "ConsultarExistencias.frx":89F5
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   375
         Width           =   3210
      End
      Begin VB.TextBox txtDato 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3750
         TabIndex        =   0
         Top             =   375
         Width           =   9060
      End
      Begin MSComctlLib.ProgressBar Barra_Prg 
         Height          =   120
         Left            =   3750
         TabIndex        =   8
         Top             =   855
         Width           =   9060
         _ExtentX        =   15981
         _ExtentY        =   212
         _Version        =   393216
         Appearance      =   0
         Max             =   1000
         Scrolling       =   1
      End
      Begin VB.Label lblTeclado 
         BackColor       =   &H8000000A&
         BackStyle       =   0  'Transparent
         Height          =   750
         Left            =   9315
         MousePointer    =   99  'Custom
         TabIndex        =   10
         Top             =   225
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Criterios de busqueda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   0
         Width           =   3015
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00404040&
         X1              =   2250
         X2              =   13650
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Image bteclado 
         Height          =   600
         Left            =   13035
         MouseIcon       =   "ConsultarExistencias.frx":89F7
         MousePointer    =   99  'Custom
         Picture         =   "ConsultarExistencias.frx":8D01
         Stretch         =   -1  'True
         Top             =   300
         Width           =   600
      End
   End
   Begin VB.CommandButton CmdBuscar 
      Appearance      =   0  'Flat
      Caption         =   "&Buscar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1090
      Index           =   0
      Left            =   14175
      Picture         =   "ConsultarExistencias.frx":8DAD
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   1680
      Width           =   1035
   End
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   0
      Top             =   12240
   End
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   9600
      Left            =   120
      TabIndex        =   5
      Top             =   2880
      Width           =   15105
      _ExtentX        =   26644
      _ExtentY        =   16933
      _Version        =   393216
      BackColor       =   16448250
      ForeColor       =   3355443
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   15658734
      ForeColorSel    =   0
      BackColorBkg    =   16448250
      GridColor       =   13421772
      WordWrap        =   -1  'True
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      FillStyle       =   1
      GridLinesFixed  =   0
      ScrollBars      =   2
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox Medir 
      Height          =   375
      Left            =   12600
      ScaleHeight     =   315
      ScaleWidth      =   315
      TabIndex        =   11
      Top             =   3600
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Image logoForm 
      Appearance      =   0  'Flat
      Height          =   960
      Left            =   480
      Picture         =   "ConsultarExistencias.frx":AB2F
      Top             =   600
      Width           =   960
   End
   Begin VB.Label lbl_Titulo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Consulta de Existencia de Productos en Sucursales"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00333333&
      Height          =   615
      Left            =   1800
      TabIndex        =   16
      Top             =   720
      Width           =   13335
   End
   Begin VB.Label lblInfo 
      BackColor       =   &H8000000A&
      BackStyle       =   0  'Transparent
      Height          =   375
      Left            =   12720
      MousePointer    =   99  'Custom
      TabIndex        =   15
      Top             =   5880
      Width           =   255
   End
   Begin VB.Label lblSelect 
      BackColor       =   &H8000000A&
      BackStyle       =   0  'Transparent
      Height          =   375
      Left            =   12720
      MousePointer    =   99  'Custom
      TabIndex        =   14
      Top             =   4560
      Width           =   255
   End
End
Attribute VB_Name = "ConsultarExistencias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Bteclado_Click()
    If gRutinas.PuedeObtenerFoco(CampoT) Then CampoT.SetFocus
    General.configPosTecladoManual
    TECLADO.Show vbModal
End Sub

Private Sub lbl_Titulo_Click()

End Sub

Private Sub txtDato_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        ConsultarProducto (Trim(txtDato.Text))
    End If
End Sub
Private Sub CmdBuscar_Click(Index As Integer)
    ConsultarProducto (Trim(txtDato.Text))
End Sub
Private Sub Exit_Click()
    Unload Me
End Sub
Public Function ValidarConexionesSistema(Optional ByVal pValidarRemota As Boolean = False)
    Dim temporario, apath, gpathsetup As String
    Dim Setup As String
    apath = App.Path
    Setup = App.Path & "\Setup.ini"
    gpathsetup = Setup
    temporario = sgetini2(Setup, "CONSULTAEXISTENCIA", "SRV_LOCAL", "?")
    BDDL.connectionstring = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=SA;Initial Catalog=VAD20;Data Source=" & temporario
    temporario = sgetini2(Setup, "SERVER", "SRV_REMOTE", "?")
    BDDR.connectionstring = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=SA;Initial Catalog=VAD20;Data Source=" & temporario
    If ProbarPing(NombreServidorRemoto(temporario)) = False Then
        MsgBox "Error de Conexi�n al Servidor. Verifique", vbExclamation + vbOKOnly, "Error de Conexi�n"
        End
    End If
End Function
Private Sub Form_Load()
'    MsgBox Command
    Call AjustarPantalla(Me)
    Set clsPing = New Cls_GetPing
    CargaIdioma
    cmbItemBusqueda.ListIndex = 0
    ValidarConexionesSistema
    LeerDatosConfiguracion
    Armagrilla
    LimpiaBarra
End Sub
Private Sub Form_Unload(Cancel As Integer)
    Set clsPing = Nothing
End Sub
Private Sub Timer1_Timer()
    Intervalo = Intervalo + 1
    If Intervalo = 3 Then
        Barra_Prg.Visible = False
        Timer1.Enabled = False
        Intervalo = 0
    End If
End Sub
Private Sub txtDato_Click()
    Set CampoT = txtDato
    If PosRetail.ModoTouch Then
        gRutinas.SeguirText CampoT, bteclado, 300
        If gRutinas.PuedeObtenerFoco(CampoT) Then CampoT.SetFocus
        General.configPosTecladoManual
        TECLADO.Show vbModal
    End If
End Sub
