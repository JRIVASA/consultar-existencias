Attribute VB_Name = "Conexion"
Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long
Public Function sgetini2(SInifile As String, SSection As String, sKey As String, SDefault As String) As String
    Dim sTemp As String
    Dim NLegth As Integer
    sTemp = Space$(256)
    NLegth = GetPrivateProfileString(SSection, sKey, SDefault, sTemp, 255, SInifile)
    sgetini2 = Left$(sTemp, NLegth)
End Function
