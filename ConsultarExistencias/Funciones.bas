Attribute VB_Name = "Funciones"
'Variable para la Dll
Public clsPing As Cls_GetPing
Public BDDL As New ADODB.connection
Public BDDR As New ADODB.connection
Public connectionstring As String
Public Intervalo As Integer
Public connection As New ADODB.connection

Public Function ConsultarProducto(ByVal Valor As String) As String
    
    On Error GoTo error
    
    Dim Rs As New ADODB.Recordset
    Dim Rs2 As New ADODB.Recordset
    Dim Contador As Integer
    Dim SQL As String
    Dim Cnn As String
    
    If Trim(Valor) = "" Or Len(Trim(Valor)) < 3 Then
        
        MsgBox "Debe Insertar un C�digo o Descripci�n seg�n sea el caso. Verifique", vbExclamation + vbOKOnly, "Advertencia"
        txtDato.SetFocus
        
    Else
        
        '   ERIC RUIZ 2017-09-06
        Armagrilla
        LimpiaBarra
        Contador = 1
        
        '   ACA CONSULTO LAS IP DE LAS SUCURSALES ASIGNADAS
        
        SQL = "SELECT c_servidor FROM VAD10.dbo.MA_SUCURSALES"
        Rs.Open SQL, BDDL.connectionstring, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not Rs.EOF Then
            '   ACA CONSULTO DE MANERA REMOTA CADA SUCUSAL PARA VER EXISTENCIAS DEL ARTICULO
            SQL = "SELECT upper(SUC.c_descripcion)                   AS Sucursal    " & GetLines & _
                  "      ,upper(DEP.c_descripcion)                   AS Ubicacion   " & GetLines & _
                  "      ,upper(PROD.c_codarticulo)                  AS Codigo      " & GetLines & _
                  "      ,upper(PROD.c_descripcion)                  AS Descripcion " & GetLines & _
                  "      ,PROD.n_cantidad - PROD.n_cant_comprometida AS Existencia  " & GetLines & _
                  "FROM   VAD10.dbo.MA_DEPOPROD    PROD  WITH (NOLOCK),             " & GetLines & _
                  "       VAD10.dbo.MA_DEPOSITO    DEP   WITH (NOLOCK),             " & GetLines & _
                  "       VAD10.dbo.MA_SUCURSALES  SUC   WITH (NOLOCK)              " & GetLines & _
                  "WHERE  PROD.c_coddeposito = DEP.c_coddeposito                    " & GetLines & _
                  "  AND  SUC.C_codigo       = SUBSTRING(DEP.c_coddeposito,1,2)     " & GetLines & _
                  "  AND (PROD.n_cantidad - PROD.n_cant_comprometida-(SELECT isnull(SUM(Cantidad),0) FROM VAD20.dbo.MA_TRANSACCION WHERE Codigo=PROD.c_codarticulo))>=0"
            '   ACA COMPLETO EL QUERY PARA VALIDAR CONSULTA POR CODIGO O DESCRIPCION
            If ConsultarExistencias.cmbItemBusqueda.ListIndex = 0 Then
                SQL = SQL & "  AND  PROD.c_codarticulo = '" & Valor & "'"
            Else
                SQL = SQL & "  AND  PROD.c_descripcion LIKE '%" & Valor & "%'"
            End If
        
            While Not Rs.EOF
                '   ACA VALIDO EL INICIO DE LA BARRA DE PROGRESO
                ConsultarExistencias.Barra_Prg.Min = 0
                ConsultarExistencias.Barra_Prg.Max = 50
                ConsultarExistencias.Barra_Prg.Visible = True
                Intervalo = 0
                Cnn = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=SA;Initial Catalog=VAD20;Data Source=" & Rs.Fields("c_servidor")
                '   ACA PROBAMOS SI EL SERVIDOR EXISTE EN LA RED
                If ProbarPing(NombreServidorRemoto(Rs.Fields("c_servidor"))) = True Then
                    Rs2.Open SQL, Cnn, adOpenForwardOnly, adLockReadOnly, adCmdText
                    If Not Rs2.EOF Then
                        While Not Rs2.EOF
        '   DE EXISTIR INFORMACION SE AGREGA EN LA GRILLA
                            ConsultarExistencias.Grid.Rows = ConsultarExistencias.Grid.Rows + 1
                            ConsultarExistencias.Grid.TextMatrix(ConsultarExistencias.Grid.Rows - 1, 1) = Rs2.Fields("Sucursal")
                            ConsultarExistencias.Grid.TextMatrix(ConsultarExistencias.Grid.Rows - 1, 2) = Rs2.Fields("Ubicacion")
                            ConsultarExistencias.Grid.TextMatrix(ConsultarExistencias.Grid.Rows - 1, 3) = Rs2.Fields("Codigo")
                            ConsultarExistencias.Grid.TextMatrix(ConsultarExistencias.Grid.Rows - 1, 4) = Rs2.Fields("Descripcion")
                            ConsultarExistencias.Grid.TextMatrix(ConsultarExistencias.Grid.Rows - 1, 5) = Rs2.Fields("Existencia")
                            Rs2.MoveNext
                        Wend
                    End If
                    Rs2.Close
                End If
                Contador = Contador + 1
                ConsultarExistencias.Barra_Prg.Value = Contador - 1
                Rs.MoveNext
            Wend
            ConsultarExistencias.Barra_Prg.Max = Contador - 1
        Else
            MsgBox "No hay Sucursales Registradas en el Sistema", vbInformation + vbOKOnly, "Servidores"
        
        End If
        Rs.Close
        ConsultarExistencias.Timer1.Enabled = True
'        ConsultarExistencias.Barra_Prg.Visible = False
    End If
error:
    
End Function

Public Function NombreServidorRemoto(ByVal NombreCompleto As String) As String
    Dim posicion As Integer
'   ACA VALIDO SI EXISTEN INSTANCIAS PEGADAS AL NOMBRE
    posicion = InStr(NombreCompleto, "\")
'   ACA VALIDO DEPENDENDO DEL RESULTDO QUE HACER
    If posicion = 0 Then
        NombreServidorRemoto = NombreCompleto
    Else
        NombreServidorRemoto = Left(NombreCompleto, posicion - 1)
    End If
End Function

Public Function ProbarPing(ByVal SucursalNombre As String) As Boolean
    On Error GoTo falloconexion
    With clsPing
'   ACA PASO LA SUCURSAL LUEGO DE FILTRARLE LA INSTANCIA
        .NombreHost = SucursalNombre 'Host
        .Npaquetes = "2" 'Cantidad de paquetes a enviar
        .Ping 'Control de salida (TextBox o Label)
        If pIPe.status = 0 Then
            ProbarPing = True
            Exit Function
        Else
            ProbarPing = False
            Exit Function
        End If
    End With
falloconexion:
    ProbarPing = False
End Function

Public Function LeerDatosConfiguracion() As String
    Dim temporario, apath, gpathsetup As String
    Dim Setup As String
    apath = App.Path
    Setup = App.Path & "\Setup.ini"
    gpathsetup = Setup
    temporario = sgetini2(Setup, "CONSULTAEXISTENCIA", "COMBOACTIVO", "?")
    If Len(Trim(temporario)) = 1 Then
        If temporario = 0 Then ConsultarExistencias.cmbItemBusqueda.Enabled = False
        If temporario = 1 Then ConsultarExistencias.cmbItemBusqueda.Enabled = True
    End If
    temporario = ""
    temporario = sgetini2(Setup, "CONSULTAEXISTENCIA", "DEFAULT", "?")
    If Len(Trim(temporario)) = 1 Then
        If temporario = 0 Then ConsultarExistencias.cmbItemBusqueda.ListIndex = 0
        If temporario = 1 Then ConsultarExistencias.cmbItemBusqueda.ListIndex = 1
    End If
End Function

Public Function Armagrilla()
    With ConsultarExistencias.Grid
        .Clear
        .Cols = 6
        .Rows = 1
        .TextMatrix(0, 1) = Stellar_Mensaje(742)
        .TextMatrix(0, 2) = Stellar_Mensaje(743)
        .TextMatrix(0, 3) = Stellar_Mensaje(212)
        .TextMatrix(0, 4) = Stellar_Mensaje(213)
        .TextMatrix(0, 5) = Stellar_Mensaje(258)
        .ColWidth(0) = 50
        .ColWidth(1) = 2500
        .ColWidth(2) = 3300
        .ColWidth(3) = 1500
        .ColWidth(4) = 6000
        .ColWidth(5) = 1500
        .RowHeight(0) = 360
        .RowHeightMin = 720
    End With
End Function

Public Function LimpiaBarra()
    With ConsultarExistencias.Barra_Prg
        .Min = 0
        .Max = 1
        .Value = 0
    End With
End Function

Public Function CargaIdioma()
    With ConsultarExistencias
        .Label1.Caption = Stellar_Mensaje(740)
        .CmdBuscar(0).Caption = Stellar_Mensaje(118)
        .lbl_Titulo.Caption = Stellar_Mensaje(741)
    End With
'   ACA LLENO EL COMBOBOX
    With ConsultarExistencias.cmbItemBusqueda
        .Clear
        .AddItem Stellar_Mensaje(212)
        .AddItem Stellar_Mensaje(213)
    End With
End Function
