VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form NoExiste 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   1905
   ClientLeft      =   15
   ClientTop       =   -30
   ClientWidth     =   8190
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   1905
   ScaleWidth      =   8190
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   500
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   8295
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   360
         TabIndex        =   5
         Top             =   90
         Width           =   7455
      End
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   60
      TabIndex        =   0
      Top             =   570
      Width           =   8085
      Begin MSFlexGridLib.MSFlexGrid GrdFocus 
         Height          =   30
         Left            =   7680
         TabIndex        =   3
         Top             =   360
         Width           =   135
         _ExtentX        =   238
         _ExtentY        =   53
         _Version        =   393216
      End
      Begin VB.CheckBox CmdMoreInfo 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000002&
         Height          =   630
         Left            =   7320
         Picture         =   "NoExiste.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   2
         ToolTipText     =   "F3"
         Top             =   480
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Image Image1 
         Height          =   600
         Left            =   90
         Picture         =   "NoExiste.frx":1D82
         Top             =   195
         Width           =   600
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "#"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   840
         Left            =   810
         TabIndex        =   1
         Top             =   225
         Width           =   7080
      End
   End
End
Attribute VB_Name = "NoExiste"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public InfoAdicional As String

Private Sub CmdMoreInfo_Click()
    
    If CmdMoreInfo.Value = vbChecked Then
        CmdMoreInfo.Value = vbUnchecked
        Exit Sub
    End If
    
    '
    
    frmMostrarXml.Caption = "Información Adicional - Stellar isPOS. Presione F12 para Salir."
    frmMostrarXml.TextArea.Text = InfoAdicional  'RemoverAcentos()
    frmMostrarXml.Show vbModal
    
    '
    
    CmdMoreInfo.Value = vbUnchecked
    
    If GRutinas.PuedeObtenerFoco(GrdFocus) Then GrdFocus.SetFocus
    
End Sub

Private Sub CmdMoreInfo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 27 Or KeyCode = vbKeyF12 Then
        Unload Me
        Set NoExiste = Nothing
    ElseIf KeyCode = vbKeyF3 Then
        If CmdMoreInfo.Visible Then CmdMoreInfo_Click
    End If
End Sub

Private Sub Form_Activate()
    If InfoAdicional <> "" Then
        CmdMoreInfo.Visible = True
        
        'If Not Me.Caption Like "*F3*" Then Me.Caption = Me.Caption & " Pulse F3 para mas información."
        
        If GRutinas.PuedeObtenerFoco(GrdFocus) Then GrdFocus.SetFocus
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 27 Or KeyCode = vbKeyF12 Then
        Unload Me
        Set NoExiste = Nothing
    ElseIf KeyCode = vbKeyF3 Then
        If CmdMoreInfo.Visible Then CmdMoreInfo_Click
    End If
End Sub

Private Sub Form_Load()
    'NoExiste.Caption = stellar_mensaje(221, True) 'LoadResString(221)
    Label2.Caption = stellar_mensaje(221, True)
    Label2.Caption = ""
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set NoExiste = Nothing
End Sub

Private Sub GrdFocus_KeyDown(KeyCode As Integer, Shift As Integer)
    Form_KeyDown KeyCode, Shift
End Sub

Private Sub Image1_Click()
    Call Form_KeyDown(27, 1)
End Sub
