Attribute VB_Name = "Variables"
Public Const gCodProducto      As Long = 853
Global Const gNombreProducto = "Stellar POS"
Global gPK As String

Public Const BkColorFrame As Long = &HFFFFFF
Public Const BkColorFrameHigh As Long = &H8000000D

Public Type StructPos
    Cliente_Cod                                         As String
    Cliente_Des                                         As String
    Cliente_PermitirBuscarMaPagos                       As Boolean
    Cliente_BusquedaNumerica                           As Boolean ' Para que cuando se busque el cliente lo tranforme numerico
    Cliente_BusquedaLongitudCadena                     As Integer ' Cuantos Caracteres va Capturar desde la Posicion Inicial, si es 0 es toda la cadena
    Cliente_BusquedaPosInicial                         As Integer ' Desde que posicion Leera el codigo de los clientes

    PosNo                                               As String
    Turno                                               As Long
    sucursal                                            As String
    
    ImprimirAgrupado                                    As Boolean
    ImprimirOrdenado                                    As Boolean
    ImprimirLineasAdicionalesFinal                      As Integer ' este para cuando termine todas las facturas
    ImprimirLineasAdicionalesFinalfacturaContinua       As Integer ' este para cuando termine cada ticket en forma continua
    ImprimirSimultaneo                                  As Boolean
    ImprimirReintegro                                   As Boolean 'Si quiere Imprimir el Reintegro Total
    ImprimirEspera                                      As Boolean 'Si quiere Imprimir el Ticket en Espera
    ValidarTeclaInsert                                  As Boolean
    SorteoProgramadoLocal                               As Boolean
    CambioPremio_PuntosCampoBase                         As String  'Campo de la Factura en base
                                                                   'al cual Acumula Puntos
    Sorteo_VerificacionCampoBase                        As String  'Campo de la Factura segun el cual
                                                        'se Verifica si aplica como Ganador del Sorteo.
    
    Imprimir_ImprimirCabecero                           As Boolean 'Si imprime el cabecero Stellar, por defecto es cierto
    Imprimir_NumLineablanco                             As Integer 'Numero de Lineas en blanco al empezar el cabecero
    
    RtfFacturacion                                      As String
    RtfFacturacionPuerto                                As Boolean
    
    RtfReintegro                                        As String
    RtfReintegroPuerto                                  As Boolean
    
    RtfDevoluciones                                     As String
    RtfDevolucionesPuerto                               As Boolean
    
    RtfZ                                                As String
    RtfZPuerto                                          As Boolean
    
    RtfEspera                                           As String
    RtfEsperaPuerto                                     As Boolean
    AgruparDenominaciones                               As Boolean
    NotificarPreciosCero                                As Boolean
    
    LecturaScanner                                      As Boolean
    ScannerControlRTSHand                               As Boolean
    ScannerPosInicioCodBar                              As Integer
    ScannerNumeroCiclosparaCaptura                      As Integer
    
    
    BalanzaTeclaAuto                                    As Boolean
    BalanzaPosInicio                                    As Integer
    BalanzaControlRTSHand                               As Boolean
    BalanzaTeclaAlfinalComando                          As Integer
    BalanzaInhabilitaScannerenCampoCantidad             As Boolean
    BalanzaManejaPuntoDecimal                           As Boolean
    'montes
    BalanzaValidarPeso                                  As Boolean
    
    MaestroEtiquetaPrecioIncluyeIva                     As Boolean
    ValidarVuelto                                       As Boolean
    ManejaFondoCaja                                     As Boolean
    ManejaMultiMoneda                                   As Boolean
    ImprimirProductosconIva                             As Boolean
    CodigoBloquearTeclado                               As Boolean
    GavetaAbiertaChequear                               As Boolean
    ProductoInformativoInhabilitaScannerenCampoMonto    As Boolean
    ContrasenaProductoInformativo                       As Boolean
    ProductoInformativoCalcularIva                      As Boolean
    ValidarRifClienteContado                            As Boolean
    ValidarNombreClienteContado                         As Boolean
    ValidarDireccionCliente                             As Boolean
    
    NivelCodigoTeclado                                  As Integer
    TipoPos_TecladoManual                               As Integer
    Producto_MaximoProductoxBusqueda                    As Integer ' Es para Limpiar el Numero de Producto en la actualizacion, si no se agrega el que son 30, si se agrega 0 no contempla Nada
    
    VerificacionElectronica_Maneja                      As Integer
    VerificacionElectronica_NivelAnulacion              As Integer
    VerificacionElectronica_ConsorcioAprobacion         As Integer ' 0 es credicard, 1 Megasoft
    VerificacionElectronica_ImpresionOrden              As Integer
    VerificacionElectronica_PausaEntreBauches           As Integer
    
    ImpresoraFiscal                                     As Boolean
    ImpresoraFiscal_ImprimeSorteo                       As Boolean
    
    ValidarMontoMaximo                                  As Boolean
    MostrarCodigoProFactura                             As Boolean
    
    CodigoMonedaVuelto                                  As String
    
    DepartamentosCaja()                                 As String
    DepartamentoRestringirCantidad                      As String
    DepartamentoMaxCantidad                             As Single
    
    ReporteDeCierre                                     As String
    EventoDeCierre                                      As String
    AplicacionExterna                                   As String
    AplicacionExternaConParametros                      As String
        
    '** MULTI IDIOMA
    ManejaMultiIdioma As Boolean
    '**
    
    '** VALIDAR EXISTENCIA DEL CAMPO EMAIL EN MA_PAGOS
    ExisteCampoEmail As Boolean
    '**
    
    'Otros
    
    MostrarMsjErrorDispositivos As Boolean
    ModoTouch As Boolean
    
End Type


Global Inicio_Transaccion As Boolean
Global FormaTotal   As Boolean
Global Prod_Pesado        As Boolean
Global PagoEfect    As Boolean
Global Procesar     As Boolean
Global RESP         As Boolean
Global Aceptado     As Boolean
Global Aceptada_Key     As Boolean
Global Llave_Activada   As Boolean
Global Parcial          As Boolean
Global Escape           As Boolean
Global Red              As Boolean
Global gScanner         As Boolean
Global gDisplay         As Boolean
Global gGabetas         As Boolean
Global gGabetaP         As Boolean
Global gPrinter         As Boolean
Global gBalanza         As Boolean
Global gGControl        As String
Global gConfig          As String
Global Agrupar          As Boolean
Global Join_Port        As Boolean
Global Tecla_Pulsada    As Boolean
Global gString          As Boolean
Global gHojilla         As Boolean
Global Ganador_Sorteo   As Boolean
Global gTot             As Double
Global Forma_Continua   As Boolean
Global Imp_Incluido     As Boolean
Global Pos_Digital      As Boolean
Global OLEPOS           As Boolean
Global oScanner         As Boolean
Global oDisplay         As Boolean
Global oGabetas         As Boolean
Global oPrinter         As Boolean
Global oBalanza         As Boolean
Global SQL_Conectado    As Boolean
Global SQL_Remoto       As Boolean
Global Cal_Precio       As Boolean
Global B_DoEvents       As Boolean
Global PermiteCantidad  As Boolean
Global FrmClienteActiva As Boolean
Global AbrirPorDisplay  As Boolean
Global MaxLongTxtCodigo As Integer
Global NivLongTxtCodigo As Integer
Global NivelxCantidad   As Integer

Global Setup As String

Global VOPOSD        As String
Global VOPOSG        As String
Global VOPOSI        As String
Global VOPOSS        As String
Global VOPOSBM       As String
Global VOPOSBA       As String
Global VOPOSL        As String

Global Srv_Local     As String
Global Srv_Remoto    As String

Global Srv_Remote_Login         As String
Global Srv_Remote_Password      As String
Global Srv_Local_Login          As String
Global Srv_Local_Password       As String

Global sucursal      As String
Global Conexion      As New ADODB.connection
Global Puerto_Sql    As Integer
Global lSecuencia    As String

Global Consecut     As String
Global NomEs        As String

' Declaración de Recordset ADO
Global rsCajaRed        As New ADODB.Recordset '*** utilizado
Global RsTrCaja         As New ADODB.Recordset '*** utilizado
Global rsCajaLocal      As New ADODB.Recordset

Global rsProductos      As New ADODB.Recordset
Global rsCodigos        As New ADODB.Recordset
Global rsTransaccion    As New ADODB.Recordset
Global RsTransaccionRed As New ADODB.Recordset

Global rsPagos          As New ADODB.Recordset
Global rsPagosRed          As New ADODB.Recordset
Global rsEureka         As New ADODB.Recordset
Global rsDevoluciones   As New ADODB.Recordset
Global rsDevolucionRed   As New ADODB.Recordset
Global rsTrDevoluciones As New ADODB.Recordset
Global rsTrDevolucionRed As New ADODB.Recordset

Global rsUsuarios       As New ADODB.Recordset
Global rsDetPago        As New ADODB.Recordset
Global rsDetPagoRed     As New ADODB.Recordset
Global rsReintegros     As New ADODB.Recordset
Global rsDetReintegros  As New ADODB.Recordset
Global rsDatDevol       As New ADODB.Recordset
Global rsDetalleEspera  As New ADODB.Recordset
Global rsDetalleTemp    As New ADODB.Recordset
Global rsImagen         As New ADODB.Recordset
Global rsConsecutivos   As New ADODB.Recordset
Global rsConsecutivosRed As New ADODB.Recordset
Global rsConsec         As New ADODB.Recordset
Global rsClientesTemp   As New ADODB.Recordset
Global RsDenomina       As New ADODB.Recordset
Global Utility          As New ADODB.Recordset
Global RsMoneda         As New ADODB.Recordset
Global RsBanco          As New ADODB.Recordset
Global RsProductoTemp   As New ADODB.Recordset
Global rsMaTransaccionTemp As New ADODB.Recordset
Global rsTrTransaccionTemp As New ADODB.Recordset
Global RsEmpresa        As New ADODB.Recordset
Global rsclientes       As New ADODB.Recordset
Global rsLocalidades    As New ADODB.Recordset
Global rsSorteos        As New ADODB.Recordset

'Declaración de Configuración
Global Limi     As String
Global Linea    As String
Global Devo     As String
Global Esper    As String
Global VC       As String
Global VP       As String
Global LoginName As String


Global Const VK_SHIFT = &H10
Global mTam_Papel As Integer

Global mPcount As Integer
Global CrLf As String * 2
Global ImpData As String
Global Forma_Total As Boolean

Global Totalizar_Toolbar_BotonGrabarTextoAlternativo As String

Global Const Sql_Will_Open = 6
Global Const Sql_Open = 7
Global Const Sql_Error = 9
Global Const Sql_Close = 0

Global AutorizadoPorSupervisor As Boolean

'Octavio 31 10 2001
Global Conectado_Linea As Boolean

'Global gNoimprimeCabecera As Boolean
'Global gImprimeNumLineablanco As Integer

Global ExistenTablas_BloqueodeProductos As Boolean

Public Enum Racionamiento_Liberar_Alcance
    ProductoIngresado = 1
    CodigoProducto = 2
    Rubro = 3
    Compra = 4
    SesionPOS = 5
End Enum

Public TmpUsuarioAutorizacion As String
Public Racionamiento_Liberar_TmpVarID As New Collection

Global gModulosBiometricos As Object
Global ManejaHiCC As Boolean

Global BioRac_NivelAutorizacion             As Integer
Global BioRac_ListaUsuariosAutorizantes     As New Collection
Global BioRac_ListaIdentidadesAutorizadas   As New Collection

Public Enum Racionamiento_CompraMinima_Modalidades
    PorCantidadDeProductos = 1
    PorCantidadDeProductosVariados = 2
    PorValorMonetario = 3
End Enum

Global Racionamiento_ManejaCompraMinima             As Boolean
Global Racionamiento_CompraMinima_Modalidad         As Racionamiento_CompraMinima_Modalidades
Global Racionamiento_CompraMinima_Cantidad          As Double
Global Racionamiento_CompraMinima_Mensaje           As String
Global Racionamiento_CompraMinima_MensajePorDefecto As String

Public Type PB_PS_ResultTicket
    
    MaxCharsPerLine             As Long
    BaseFilePath                As String
    OptionalHeaderFilePath      As String
    
    TmpFilePath                 As String
    TmpHeader                   As String
    
End Type

Public Type PB_PS_ProductTypeCodes
    
    LongDistanceStellarCode As String
    PinlessStellarCode As String
    DomesticTopupStellarCode As String
    InternationalTopupStellarCode As String
    SunpassStellarCode As String
    
End Type

Public Type PrepaidBlackstoneSettings
    
    SerialNumber                                            As String
    ServiceVersionNumber                                    As String
    
    PrepaidRootDirectory                                    As String
    
    PhoneServices_ImageDirectory                            As String
    PhoneServices_DataDirectory                             As String
    PhoneServices_TopSoldProductsDirectory                  As String
    
    StellarProductCode                                      As Long
    
    ProductCodes                                            As PB_PS_ProductTypeCodes
    
    ResultTicket                                            As PB_PS_ResultTicket
    
    Active                                                  As Boolean
    
End Type

Public PrepaidBlackstone As PrepaidBlackstoneSettings

Public Type RNC_Ticket
    
    MaxCaracteresLinea              As Long
    RutaBaseArchivoTicket           As String
    RutaBaseArchivoCierre           As String
    ArchivoCabeceroOpcional         As String
    ImpresionFiscal                 As Boolean
    
    TmpArchivo                      As String
    TmpCabecero                     As String
    
End Type

Public Type RecargasNovaredConfiguracion
    
    StellarProductCode                                      As Long
    
    Activo                                                  As Boolean
    
    Usuario                                                 As String
    Clave                                                   As String
    POSId                                                   As String
    
    DirectorioRaiz                                          As String
    SubDirectorioImagenes                                   As String
    
    ProductoStellar_General                                 As String
    ProductoStellar_Especifico                              As Collection
    ProductoStellar_ListaCodigos                            As String
    
    NivelAccesoServiciosPrepago                             As Integer
    NivelCierreServiciosPrepago                             As Integer
    CodigoOperacionAutorizadaAccesoServiciosPrepago         As String
    CodigoOperacionAutorizadaCierreServiciosPrepago         As String
    
    TmpInfoProducto                                         As Object
    
    TmpTipoServicio                                         As RNC_TipoServicio
    TicketResultado                                         As RNC_Ticket
    
End Type
Global CampoT       As Object
Global CadenaConexionDefault_VAD10 As String
Global CadenaConexionDefault_VAD20 As String
Global CadenaConexionDefault_POS_LOCAL As String
Global CadenaConexionDefault_ADM_LOCAL As String

Global CadenaConexionShapeDefault_VAD10 As String
Global CadenaConexionShapeDefault_VAD20 As String
Global CadenaConexionShapeDefault_POS_LOCAL As String
Global CadenaConexionShapeDefault_ADM_LOCAL As String

Public DebugMode As Boolean
Global PosRetail            As StructPos
