Attribute VB_Name = "General"
Public gRutinas As New cls_Rutinas
Private Const mMaxWidthGrid As Long = 9480
Private Const mMinWidthGrid As Long = 5880

Public Enum Alineacion
    flexDefault = -1
    flexAlignLeftTop = 0
    flexAlignLeftCenter = 1
    flexAlignLeftBottom = 2
    flexAlignCenterTop = 3
    flexAlignCenterCenter = 4
    flexAlignCenterBottom = 5
    flexAlignRightTop = 6
    flexAlignRightCenter = 7
    flexAlignRightBottom = 8
    flexAlignGeneral = 9
End Enum

Public Enum AfectarVariables
    Si = True
    NO = False
End Enum

'INICIO VARIABLES / PROCEDIMIENTOS PARA OBTENER LA ALTURA (HEIGHT) DE LA BARRA DE TAREAS

Private Const ABM_GETTASKBARPOS = &H5

Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Private Type APPBARDATA
    cbSize As Long
    hwnd As Long
    uCallbackMessage As Long
    uEdge As Long
    rc As RECT
    lParam As Long
End Type

Private Declare Function SHAppBarMessage Lib "shell32.dll" (ByVal dwMessage As Long, pData As APPBARDATA) As Long

Private Declare Function GetDeviceCaps Lib "gdi32" _
        (ByVal hdc As Long, ByVal nIndex As Long) As Long

Private Const SYNCHRONIZE = &H100000
Private Const INFINITE = -1&

Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, _
ByVal bInheritHandle As Long, ByVal dwProcessID As Long) As Long
Private Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long

Const HorizontalResolution = 8
Const VerticalResolution = 10

Public Function GetTaskBarHeight() As Long
    Dim ABD As APPBARDATA

    SHAppBarMessage ABM_GETTASKBARPOS, ABD
    
    GetTaskBarHeight = ABD.rc.Bottom - ABD.rc.Top

    'MsgBox "Width:" & ABD.rc.Right - ABD.rc.Left
    'MsgBox " Height:" & ABD.rc.Bottom - ABD.rc.Top

End Function

Public Sub configPosTecladoManual()

    Select Case PosRetail.TipoPos_TecladoManual
        ' Posicion por Defecto: Centro de la pantalla (CenterScreen)
        Case 0
            TECLADO.ParentForm_PosT = 0
            TECLADO.ParentForm_PosH = 0
            TECLADO.PosT = 0
            TECLADO.PosH = 0
        ' Posicion Centrado al Fondo (CenterBottom)
        Case 1
            ' Como el valor por defecto es el centro de la pantalla, ya no necesito cambiar
            ' la posicion horizontal del teclado, solo calculo la posici�n para colocarlo en el fondo
            TECLADO.ParentForm_PosT = 0
            TECLADO.ParentForm_PosH = 0
            'TECLADO.PosT = Screen.Height - TECLADO.Height - (Screen.TwipsPerPixelY * GetTaskBarHeight)
            'MsgBox TECLADO.PosT & " - " & ((GetDeviceCaps(TECLADO.hdc, VerticalResolution) * Screen.TwipsPerPixelY) - TECLADO.Height - (Screen.TwipsPerPixelY * GetTaskBarHeight))
            TECLADO.PosT = (GetDeviceCaps(TECLADO.hdc, VerticalResolution) * Screen.TwipsPerPixelY) - TECLADO.Height - (Screen.TwipsPerPixelY * GetTaskBarHeight)
            TECLADO.PosH = 0
        Case 2
            'Debajo de un control especifico
            'Under Development
            PosRetail.TipoPos_TecladoManual = 0
        Case 3
            'Encima de un control especifico
            'Under Development
            PosRetail.TipoPos_TecladoManual = 0
    End Select

End Sub

'FIN VARIABLES / PROCEDIMIENTOS PARA OBTENER LA ALTURA (HEIGHT) DE LA BARRA DE TAREAS


'
' GRABAR CONSECUTIVO DISPONIBLE PARA ESE MOMENTO
'
Public Function Ceros(intValue, intDigits) As String
    If intDigits - Len(intValue) > 0 Then
        Ceros = String(intDigits - Len(intValue), "0") & intValue
    Else
        Ceros = intValue
    End If

'    Ceros = String(intDigits - Len(intValue), "0") & intValue
End Function

Public Function Blancos(intValue, intDigits) As String
    If intDigits - Len(intValue) > 0 Then
        Blancos = String(intDigits - Len(intValue), " ") & intValue
    Else
        Blancos = intValue
    End If

'    Blancos = String(intDigits - Len(intValue), " ") & intValue
End Function
Public Sub Close_Rec(Reg As ADODB.Recordset)
    If Reg.State = adStateOpen Then Reg.Close
    Set Reg = Nothing
End Sub
Public Function SDecimal() As String
    Dim numero As Double, Caracteres As String
    numero = 123.123
    Caracteres = Trim(CStr(numero))
    SDecimal = Mid(Caracteres, 4, 1)
End Function

Public Sub SetNet(Estatus As Boolean)
    SQL_Remoto = Estatus
    SQL_Conectado = Estatus
'    Call CAPTION_ESTADO
End Sub
Public Sub stellar_mensajeText(Modo As Boolean, Cadena As String)
    NoExiste.Label1.Caption = Cadena
    NoExiste.Show vbModal
End Sub
Public Function StringToHex(ByVal StrToHex As String) As String
    
    Dim strTemp   As String
    Dim StrReturn As String
    Dim i         As Long
    
    For i = 1 To Len(StrToHex)
        strTemp = Hex$(Asc(Mid$(StrToHex, i, 1)))
        If Len(strTemp) = 1 Then strTemp = "0" & strTemp
        StrReturn = StrReturn & Space$(1) & strTemp
    Next i
    
    StringToHex = StrReturn
    
End Function

Public Function ReadFileIntoString(StrFilePath As String) As String
    
    On Error GoTo ErrFile

    Dim FSObject As New FileSystemObject
    Dim TStream As TextStream

    Set TStream = FSObject.OpenTextFile(StrFilePath)
    If Not TStream.AtEndOfStream Then ReadFileIntoString = TStream.ReadAll

    Exit Function
    
ErrFile:
    
    ReadFileIntoString = ""
    
End Function

Public Function WriteStringIntoFile(StrFilePath As String, pText As String) As Boolean
    
    On Error GoTo ErrFile

    Dim FSObject As New FileSystemObject
    Dim TStream As TextStream

    Set TStream = FSObject.CreateTextFile(StrFilePath, True, False)
    TStream.Write (pText)
    TStream.Close
    
    WriteStringIntoFile = True

    Exit Function
    
ErrFile:
    
    WriteStringIntoFile = False
    
End Function
