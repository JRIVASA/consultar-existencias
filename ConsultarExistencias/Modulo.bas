Attribute VB_Name = "Principal"
'Used for DisableCtrlAltDelete

Private mClsTmp As Object

Public Parametros               As String
'Public gMensajes                As New Cls_Mensajes
'Public gReportes                As New Cls_Reportes
'Public gCalculadora             As New Cls_Calculadora
'Public gChequeoSistema          As New Cls_ChequeoSistema

Private mPromPinPad                 As String

Public Const EspaciosCaption As Long = 65
Private Declare Function SystemParametersInfo Lib _
"user32" Alias "SystemParametersInfoA" (ByVal uAction _
As Long, ByVal uParam As Long, ByVal lpvParam As Any, _
ByVal fuWinIni As Long) As Long

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpString _
    As Any, ByVal lpFileName As String) As Long

'--------------------------------------
Declare Function ExitWindowsEx& Lib "user32" (ByVal uFlags&, ByVal dwReserved&)
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

'///////////////////////////////////////////////////////////////////////////////////////
' I N I C I O     A P L I C A C I O N
''///////////////////////////////////////////////////////////////////////////////////////

Sub Main()
    
    DebugMode = Dir(App.Path & "\POSDebugMode.Active") <> vbNullString
    
    Dim mMaxLongTxtCodigo As Long
    
    'CONFIGURACION REGIONAL
    'If Not VerificacionConfiguracionNumericadelEquipo("La Aplicacion se Cerrara en este equipo.") Then End
    
    mPcount = 13 ' Comienza despues de la cabecera
    'DisableCtrlAltDelete (True)
    
    Call gRutinas.CtrlAltDeleteKeys(True)
    
    B_DoEvents = False
    
    If App.PrevInstance = True Then
        'Call stellar_mensaje(True, 0)
        Call Stellar_Mensaje(0)
        End
    End If
    
    PagoEfect = False
    'DisableCtrlAltDelete (False)
    Procesar = False
    Trans = False
    Parcial = False
    Cant = 1
    Tot = 0
    Subt = 0
    Impu = 0
    mTam_Papel = 40
    
    gPK = Chr(83) & Chr(81) & Chr(76) & "_" _
    & Chr(73) & Chr(55) & Chr(51) & Chr(51) _
    & Chr(55) & Chr(50) & Chr(49) & Chr(83) _
    & Chr(78) & Chr(83) & Chr(69) & Chr(74) _
    & Chr(68) & Chr(75) & Chr(78) & "-" _
    & Chr(57) & Chr(54) & Chr(66) & Chr(71) _
    & Chr(52) & Chr(44) & Chr(48) & "_" & Chr(90)
    
    '********************************
    '*** UBICA EL ARCHIVO SETUP.INI
    '********************************
    
Reintentar:
    
    Setup = App.Path & "\setup.ini"
    
    TiempoEsperaInicializar = sGetIni(Setup, "POS", "TiempoEsperaInicializar", "3")
    If IsNumeric(TiempoEsperaInicializar) Then Sleep (CLng(TiempoEsperaInicializar * 1000))
   
    PosRetail.DepartamentoMaxCantidad = Val(sGetIni(Setup, "POS", "MaximaCantidadDepartamentoPos", ""))
    PosRetail.DepartamentoRestringirCantidad = sGetIni(Setup, "POS", "DepartamentoRestringirCantidad", "")
    mDptoPos = Split(sGetIni(Setup, "POS", "DepartamentoPos", ""), "|")
    
    ReDim PosRetail.DepartamentosCaja(0)
    
    If UBound(mDptoPos) >= 0 Then
        For i = 0 To UBound(mDptoPos)
            ReDim Preserve PosRetail.DepartamentosCaja(i)
            PosRetail.DepartamentosCaja(i) = mDptoPos(i)
        Next i
    End If
    
    MaxLongTxtCodigo = 0
    mMaxLongTxtCodigo = Val(sGetIni(Setup, "POS", "CodigoLongitudMaxima", 0))
    
LeerConfiguracion:
    
    Ubicar = Dir(Setup)
    
    If LCase(Ubicar) <> "setup.ini" Then
        frmDatos.TipoApp = POS_Retail
        frmDatos.fSetup = Setup
        frmDatos.Show vbModal
    End If
    
    If Retorno Then DoEvents: gRutinas.DormirSistema (1): GoTo LeerConfiguracion

      

    
    '*******************************************
    '*** UBICA DEL PUERTO DE SQL
    '*******************************************
    
    Dim rPuerto_Sql As String
    
    rPuerto_Sql = sGetIni(Setup, "Port", "Port", "?")
    
    If rPuerto_Sql = "?" Then
        Call Stellar_Mensaje(4, False)
        End
    ElseIf Not IsNumeric(rPuerto_Sql) Then
        Call Stellar_Mensaje(5, False)
        End
    End If
    
    Puerto_Sql = CDbl(rPuerto_Sql)
        
    ' MANEJA MULTI IDIOMA
    
    PosRetail.ManejaMultiIdioma = Val(sGetIni(Setup, "POS", "PermitirMultiIdioma", "0")) = 1
    
    PosRetail.MostrarMsjErrorDispositivos = Val(sGetIni(Setup, "POS", "MostrarMsjErrorDispositivos", "1")) = 1
        
    With PrepaidBlackstone
        
        .StellarProductCode = gCodProducto
        
        .SerialNumber = sGetIni(Setup, "BlackstonePrepaid", "SerialNumber", "")
        .ServiceVersionNumber = sGetIni(Setup, "BlackstonePrepaid", "ServiceVersionNumber", "1")
        .PrepaidRootDirectory = sGetIni(Setup, "BlackstonePrepaid", "RootDirectory", vbNullString) ' Algo como \\NetworkFolder\BlackstonePrepaid
        .PhoneServices_DataDirectory = sGetIni(Setup, "BlackstonePrepaid", "PhoneServices_DataDirectory", vbNullString) ' Algo como \\NetworkFolder\BlackstonePrepaid\Data\Phone Services
        .PhoneServices_ImageDirectory = sGetIni(Setup, "BlackstonePrepaid", "PhoneServices_ImageDirectory", vbNullString) ' Algo como \\NetworkFolder\BlackstonePrepaid\Images\Phone Services
        .PhoneServices_TopSoldProductsDirectory = sGetIni(Setup, "BlackstonePrepaid", "PhoneServices_TopSoldProductsDirectory", vbNullString) ' Puede ser el mismo que el Data Directory a menos que se requiera otra estructura. O siguiendo el mismo esquema estructural, algo como \\NetworkFolder\BlackstonePrepaid\Top Sold\Phone Services
        
        .ProductCodes.LongDistanceStellarCode = sGetIni(Setup, "BlackstonePrepaid", "LongDistanceProductCode", vbNullString)
        .ProductCodes.PinlessStellarCode = sGetIni(Setup, "BlackstonePrepaid", "PinlessProductCode", vbNullString)
        .ProductCodes.DomesticTopupStellarCode = sGetIni(Setup, "BlackstonePrepaid", "DomesticTopupProductCode", vbNullString)
        .ProductCodes.InternationalTopupStellarCode = sGetIni(Setup, "BlackstonePrepaid", "InternationalTopupProductCode", vbNullString)
        .ProductCodes.SunpassStellarCode = sGetIni(Setup, "BlackstonePrepaid", "SunpassProductCode", vbNullString)
        
        .ResultTicket.BaseFilePath = sGetIni(Setup, "BlackstonePrepaid", "ResultTicketFilePath", vbNullString) ' Algo como \\NetworkFolder\BlackstonePrepaid\ResultTickets\Phone Services\$(ComputerName)\LastTicket.txt
        ' Variables opcionales a utilizar en la ruta [ $(ComputerName): Nombre del Equipo | $(TicketNumber): Numero del Ticket ]
        .ResultTicket.BaseFilePath = Replace(.ResultTicket.BaseFilePath, "$(ComputerName)", gRutinas.NombreDelComputador)
        .ResultTicket.OptionalHeaderFilePath = sGetIni(Setup, "BlackstonePrepaid", "ResultTicketHeaderFilePath", vbNullString) ' Algo como \\NetworkFolder\BlackstonePrepaid\ResultTickets\Header.txt
        .ResultTicket.MaxCharsPerLine = Val(sGetIni(Setup, "BlackstonePrepaid", "ResultTicketMaxLineCharacters", "32"))
        
        .Active = (.SerialNumber <> vbNullString) And (.PrepaidRootDirectory <> vbNullString) And (.PhoneServices_DataDirectory <> vbNullString) And (.PhoneServices_ImageDirectory <> vbNullString) And (.PhoneServices_TopSoldProductsDirectory <> vbNullString)
        
    End With
    
    ObtenerCorrelativo_ActivarQueryViejo = (Val(sGetIni(Setup, "POS", "QueryCorrelativoOriginal", "0")) = 1)
    
    DiasMaximosParaDevolucion = Val(sGetIni(Setup, "POS", "DiasMaximosDevolucion", "-1"))
    
    ModoVerificador_ConsultaEnRed = (Val(sGetIni(Setup, "POS", "VerificarPreciosRed", "0")) = 1)
    ModoVerificador_TiempoVisualizacion = Abs(Val(sGetIni(Setup, "POS", "VerificarPrecios_TiempoVisualizacion", "4")))
    
    GavetaVentaSoloEfectivo = Val(sGetIni(Setup, "POS", "GavetaVentaSoloEfectivo", "")) = 1
    ReintegroPesableTotal = Val(sGetIni(Setup, "POS", "ReintegroPesableTotal", ""))
    
    'Jesus 20/07/2017 Plazas variable equivalente a modod touch de bz pero con teclado de pos
    PosRetail.ModoTouch = sGetIni(Setup, "POS", "ModoTouch", "0")
    
'    ImprimeSimultaneo
    
    On Error GoTo 0
    
    Procesar = True
    
    f_Login.Show vbModal
    
    Exit Sub
    
Error_Base:
    
    'Ewx_Shutdown
    
    Call Stellar_Mensaje(10, False)
    
    'lresult = ExitWindowsEx(2, 0&) 'Reset the computer
    
    Call gRutinas.ReiniciarWindows(EWX_REBOOT)
    
    End
    
End Sub

Sub DisableCtrlAltDelete(bDisabled As Boolean)
   
    ' Disables Control Alt Delete Breaking as well as Ctrl-Escape
    
    Dim X As Long
    
    X = SystemParametersInfo(97, bDisabled, CStr(1), 0)
    
End Sub

Public Function StellarMensaje(Mensaje As Integer, Optional pDevolver As Boolean = True) As String
    StellarMensaje = Stellar_Mensaje(Mensaje, pDevolver)
End Function

Public Function Stellar_Mensaje(Mensaje As Integer, Optional pDevolver As Boolean = False) As String
    
    Texto = LoadResString(Mensaje)
    Stellar_Mensaje = Texto
    
End Function

Public Function sGetIni(SInifile As String, SSection As String, sKey _
As String, SDefault As String) As String
    
    Dim sTemp As String * 10000
    Dim nLength As Integer
    
    sTemp = Space$(10000)
    nLength = GetPrivateProfileString(SSection, sKey, SDefault, sTemp, 10000, SInifile)
    sGetIni = Left$(sTemp, nLength)
    
End Function
        
Public Function WriteIni(ByVal SInifile As String, ByVal SSection As String, _
ByVal sKey As String, ByVal sValue As String) As String

    Dim sTemp As String
    Dim N As Integer

    sTemp = sValue
    
    ' Reemplazar todos los caracteres cr/lf con espacios
    'For n = 1 To Len(svalue)
        'If Mid$(svalue, n, 1) = vbCr Or Mid$(svalue, n, 1) = vbLf _
        'Then Mid$(svalue, n) = " "
    'Next
    
    N = WritePrivateProfileString(SSection, sKey, sValue, SInifile)
    
End Function

Public Function sWriteIni(ByVal SInifile As String, ByVal SSection As String, _
ByVal sKey As String, ByVal sValue As String) As String
    sWriteIni = WriteIni(SInifile, SSection, sKey, sValue)
End Function

Public Function PathExists(pPath As String) As Boolean
    'On Error Resume Next
    ' For Files it works normally, but for Directories,
    ' pPath must end in "\" to be able to find them.
     If Dir(pPath, vbArchive Or vbDirectory Or vbHidden) <> "" Then PathExists = True
End Function

Public Function GetDirectoryRoot(pPath As String) As String

    Dim POS As Long
    
    POS = InStr(1, pPath, "\")
    
    If POS <> 0 Then
        GetDirectoryRoot = Left(pPath, POS - 1)
    Else
        GetDirectoryRoot = vbNullString
    End If
    
End Function

Public Function GetDirParent(pPath As String) As String

    Dim POS As Long
    
    POS = InStrRev(pPath, "\")
    
    If POS <> 0 Then
        GetDirParent = Left(pPath, POS - 1)
    Else
        GetDirParent = vbNullString
    End If

End Function

Public Function CopyPath(Source As String, Destination As String) As Boolean
    
    On Error GoTo ErrCP
    
    Dim FSO As New FileSystemObject
    
    FSO.CopyFile Source, Destination
    
    CopyPath = True
    
    Exit Function
    
ErrCP:

    CopyPath = False
    
    'Debug.Print Err.Description
    
    Err.Clear
    
End Function

Public Function GetLines(Optional ByVal HowMany As Long = 1)
    
    Dim HowManyLines As Integer
    
    HowManyLines = HowMany
    
    For i = 1 To HowManyLines
        GetLines = GetLines & vbNewLine
    Next i
    
End Function

Public Function GetTab(Optional ByVal HowMany As Long = 1)
    
    Dim HowManyTabs As Integer
    
    HowManyTabs = HowMany
    
    For i = 1 To HowManyTabs
        GetTab = GetTab & vbTab
    Next i
    
End Function

Public Function Mensaje(Activo As Boolean, Texto As String, Optional pVbmodal = True, _
Optional txtBoton1 As Variant, Optional txtBoton2 As Variant) As Boolean
    
    Uno = Activo
    
    On Error GoTo Falla_Local
    
    If Not IsMissing(txtBoton1) Then txtMensaje1 = CStr(txtBoton1) Else txtMensaje1 = ""
    If Not IsMissing(txtBoton2) Then txtMensaje2 = CStr(txtBoton2) Else txtMensaje2 = ""
    
    If Not frm_Mensajeria.Visible Then
        
        frm_Mensajeria.Mensaje.Text = frm_Mensajeria.Mensaje.Text & IIf(frm_Mensajeria.Mensaje.Text <> "", " ", "") & Texto
        
        If pVbmodal Then
            If Not frm_Mensajeria.Visible Then
                Retorno = False
                
                frm_Mensajeria.Show vbModal
                
                Set frm_Mensajeria = Nothing
            End If
        Else
            Retorno = False
            
            frm_Mensajeria.Show
            
            frm_Mensajeria.Aceptar.Enabled = False
            
            Set frm_Mensajeria = Nothing
        End If
        
    End If
    
    Mensaje = Retorno
    
Falla_Local:

End Function

Public Sub AjustarPantalla(ByRef Forma As Form, Optional EsFormNavegador As Boolean = False)
    'Si la pantalla es igual a 1024x768px, se ajusta el alto del form para cubrir la pantalla.
    'Las medidas est�n en twips.
    If Screen.Height = "11520" And Forma.Height = 10920 Then 'And Screen.Width = "15360" Then
        Forma.Height = Screen.Height - (GetTaskBarHeight * Screen.TwipsPerPixelY)
        Forma.Top = 0
        Forma.Left = (Screen.Width / 2) - (Forma.Width / 2)
        If Forma.Left < 0 Then Forma.Left = 0
        'El form navegador tiene un footer que siempre debe quedar en bottom.
        If GetTaskBarHeight = 0 And EsFormNavegador Then
            Forma.Frame1.Top = (Screen.Height - Forma.Frame1.Height)
        End If
    Else
        'Si no es la resoluci�n m�nima de Stellar, se centra el form.
        Forma.Top = ((Screen.Height / 2) - (Forma.Height / 2))
        Forma.Left = ((Screen.Width / 2) - (Forma.Width / 2))
    End If
End Sub

Function RemoverCaracter(ByVal pString As String, ByVal pPosicion As Long) As String
    On Error Resume Next
    RemoverCaracter = pString ' En caso de error devolver el mismo.
    RemoverCaracter = Mid(pString, 1, pPosicion - 1) & Mid(pString, pPosicion + 1, Len(pString) - pPosicion)
End Function

Public Function AsEnumerable(ArrayList As Variant) As Collection
    
    Set AsEnumerable = New Collection
    
    On Error Resume Next
    
    For i = LBound(ArrayList) To UBound(ArrayList)
        AsEnumerable.Add ArrayList(i), CStr(i)
    Next i
    
End Function
' Copia en el portapapeles y Devuelve el string.

Public Function CtrlC(ByVal Text): On Error Resume Next: CtrlC = Text: Clipboard.Clear: Clipboard.SetText CtrlC: End Function

' Devuelve lo que haya en el portapapeles.

Public Function CtrlV() As String: On Error Resume Next: CtrlV = Clipboard.GetText: End Function

' Formatea Fecha Corta o Larga si incluye hora, seg�n configuraci�n regional. _
Solo debe usarse en capa de presentaci�n, no en consultas a bases de datos !

Public Function GDate(ByVal pDate) As String: On Error Resume Next: GDate = FormatDateTime(pDate, vbGeneralDate): End Function

' Formatea Fecha Corta seg�n configuraci�n regional. _
Solo debe usarse en capa de presentaci�n, no en consultas a bases de datos !

Public Function SDate(ByVal pDate) As String: On Error Resume Next: SDate = FormatDateTime(pDate, vbShortDate): End Function

Public Function ValidarConexion(pConexion As Object, Optional pNewConnectionString As String, Optional TmpCnTimeOut As Long = 4) As Boolean
    
    On Error GoTo Check
    
    ' La idea de esta funci�n es reestablecer conexiones que se asume estan
    ' en estado abierto pero internamente pudieran estar ocasionando error.
    ' Actualizaci�n: O si est� cerrada, intentar abrirla nuevamente para poder utilizarla.
    
    'Debug.Print pConexion.ConnectionString
    
    Dim mRs As ADODB.Recordset, mSql As String, Reintento As Boolean
    Dim mConnectionString As String, mConnectionTimeOut As Long, mCommandTimeOut As Long
    
    mConnectionTimeOut = pConexion.ConnectionTimeout
    mCommandTimeOut = pConexion.CommandTimeout
    
TryAgain:
    
    mSql = "SELECT 1 AS ConexionActiva"
    
    Set mRs = New ADODB.Recordset
    
    pConexion.CommandTimeout = 4
    
    DoEvents
    
    mRs.Open mSql, pConexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    'Debug.Print "Conexion Activa:", Not MRS.EOF
    
    ValidarConexion = True
    
Finally:
    
    pConexion.CommandTimeout = mCommandTimeOut
    
    Exit Function
    
Check:
    
    'Save Error properties.
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mDLLError = Err.LastDllError
    mErrorSource = Err.Source
    
    DoEvents
    
    Resume ResetErrorHandler
    
ResetErrorHandler:
    
    On Error GoTo Check
    
    If (mErrorNumber = (-2147467259) Or mErrorNumber = (3709)) And Not Reintento Then ' Error de Conexi�n / Error General de Red / Conexi�n cerrada.
        
        If pConexion.State <> adStateClosed Then pConexion.Close
        
        mConnectionString = IIf(Trim(pNewConnectionString) <> vbNullString, pNewConnectionString, pConexion.connectionstring)
        
        Debug.Print "Intentando Reestablecer conexi�n", mConnectionString
        
        Reintento = True
        
        pConexion.ConnectionTimeout = TmpCnTimeOut
        
        pConexion.Open mConnectionString
        
        Debug.Print "Conexi�n Reactivada."
        
        Reintento = False 'Abrio la conexi�n... por lo tanto, vamos a hacer otro intento de validaci�n.
        
    Else ' Ante otros errores desconocidos / de otra causa, salir.
        Reintento = True
    End If
    
    If Not Reintento Then
        GoTo TryAgain
    Else
        Debug.Print "Fallo al intentar reestablecer la conexi�n."
        If pConexion.State = adStateClosed Then pConexion.ConnectionTimeout = mConnectionTimeOut
        ValidarConexion = False
    End If
    
    GoTo Finally
    
End Function

Public Sub ValidarConexionesSistema(Optional ByVal pValidarRemota As Boolean = False)
    
    If Conectado_Linea Then
        ValidarConexion Entorno.vad10red
        ValidarConexion Entorno.vad20red
    End If
    
    ValidarConexion Entorno.vad10local
    ValidarConexion Entorno.vad20local
    
End Sub
Public Function Hex2Dec(ByVal pHexBytes As String): On Error Resume Next: Hex2Dec = CDbl("&H" & pHexBytes): End Function
